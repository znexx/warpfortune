#!/usr/bin/python3
import logging
import json
import http.client
import os
from html.parser import HTMLParser

#logging.getLogger().setLevel(logging.DEBUG)

stored_quotes_filename = os.environ['HOME'] + '/.warpfortune_next'

class QuotesFinder(HTMLParser):
    reading_quote = False
    first_line = True
    quote = ""
    quotes = []

    def __init(self):
        HTMLParser.__init__(self)

    def handle_starttag(self, tag, attrs):
        logging.debug(f"Starttag {tag}")
        if tag == "tt" and not self.reading_quote:
            self.reading_quote = True

    def handle_data(self, data):
        if self.reading_quote:
            if not self.first_line:
                self.quote = self.quote + "\n"
            self.quote = self.quote + data.strip()
            self.first_line = False

    def handle_endtag(self, tag):
        if tag == "tt" and self.reading_quote:
            self.reading_quote = False
            self.first_line = True
            logging.debug(f"Parsed forth a quote:\n{self.quote}")
            self.quotes.append(self.quote)
            self.quote = ""

def get_new_quotes ():
    logging.debug("Getting new quotes")
    conn = http.client.HTTPSConnection('warpdrive.se')
    try:
        conn.request('GET', '/slumpa')
    except socket.gaierror:
        logging.error("Could not fetch warpdrive.se/slumpa!")
    response = conn.getresponse()

    response_text = response.read().decode('latin-1')

    parser = QuotesFinder()
    parser.feed(response_text)
    return parser.quotes

def get_quotes_from_file ():
    global stored_quotes_filename
    try:
        stored_quotes_file = open(stored_quotes_filename, 'r')
        stored_quotes = json.load(stored_quotes_file)
        stored_quotes_file.close()
        return stored_quotes
    except FileNotFoundError:
        logging.error("Could not open quote file, starting from empty")
        stored_quotes_file = open(stored_quotes_filename, 'w')
        stored_quotes_file.write('[]')
        stored_quotes_file.close()
        return []
    except json.JSONDecodeError:
        logging.error("Could not parse JSON data from quote file, overwriting with new quotes")
        stored_quotes_file = open(stored_quotes_filename, 'w')
        stored_quotes_file.write('[]')
        stored_quotes_file.close()
        return []


def get_next_quote ():
    global stored_quotes_filename

    stored_quotes = get_quotes_from_file()

    # lazily read the quotes files
    logging.debug(f"number of stored quotes: {len(stored_quotes)}")

    # if we're now out of quotes, fetch new ones asynchronously asap!
    if len(stored_quotes) < 2:
        pid = os.fork()
        if pid == 0: # child
            logging.debug("time to get new quotes!")

            stored_quotes = get_quotes_from_file()

            new_quotes = get_new_quotes()
            logging.debug(f"Got {len(new_quotes)} new quotes")
            stored_quotes.extend(new_quotes)

            stored_quotes_file = open(stored_quotes_filename, 'w')
            json.dump(stored_quotes, stored_quotes_file)
            stored_quotes_file.close()

            exit(0)
        elif len(stored_quotes) == 0: # parent
            # we have no stored quotes left to show, so we'd better wait
            os.waitpid(pid, 0) # wait for child to finish
            stored_quotes = get_quotes_from_file()


    # if we read at least one quote, pop it and store for returning!
    if len(stored_quotes) >= 1:
        print(stored_quotes.pop())
        logging.debug(f"stored quotes after pop: {len(stored_quotes)}")

    # write the list of (now one fewer) quotes back to the file
    stored_quotes_file = open(stored_quotes_filename, 'w')
    json.dump(stored_quotes, stored_quotes_file)
    stored_quotes_file.close()

if __name__ == '__main__':
    get_next_quote()
