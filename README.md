# Warpfortune

A `fortune`-like script using the Swedish IRC quote-page [Warpdrive](http://warpdrive.se/) as a source of quotes.

## Features

* Simple, does what it should do. No more, no less
* Lightweight, the executable ticks in just above 3 kB and it prints a quote in around 42 ms in Debian on an i7-3770 according to cProfile
* No dependencies except Python 3
* Caches a whole page (25) of randomized quotes [from Warpdrive](http://warpdrive.se/slumpa) preemptively to always deliver a quote quickly and to reduce traffic to warpdrive.se

## Installation
1. Make sure you have Python 3 installed on your system
2. Make sure the script is executable by running `chmod +x warpfortune` on it.
3. Put `warpfortune` in your `/bin/`, `/usr/bin/`, `~/bin/`, or any other directory in your PATH environment variable.

## Usage
If you have followed the installation instructions you can just run:
```
$ warpfortune
```

If you don't have it installed, you can of course still run it as-is with one of these commands:
```
$ ./warpfortune
$ python3 warpfortune
```
